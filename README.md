# Python API #

## Description ##
This is a simple python api that is stores information on mySQL within the google cloud. This is a persistent cloud storage base.
The use of cloud database ensures that this can be run from any location. This DB holds customer information and stores it in the 
Customers table on the DB. 
### The Data Model ###
```python

{
  class Customers(db.Model):
      id = db.Column(db.Integer, primary_key = True, nullable = False)
      first_name = db.Column(db.String(50), nullable = False)
      last_name = db.Column(db.String(50), nullable = False)
      email = db.Column(db.String(100), nullable = False, unique = True)
      address = db.Column(db.String(100), nullable = False)
      city = db.Column(db.String(30), nullable = False)
      state = db.Column(db.String(20), nullable = False)
      zip_code = db.Column(db.String(7), nullable = False)
}

```

### Testing the API ###

For testing I utilized Postman and it worked pretty well so I reccomend using the same if you wish to make any changes
  and would like to test your own code.  

  #### Testing POST ####
  local path used: [local ip address can]/customers
  under body with raw json input I used the following format.
  ```json
  {
    
    "first_name" : "customer firstname",
    "last_name" : "customer lastname",
    "email" : "customer email",
    "address" : "customer address",
    "city" : "customer city",
    "state" : "customer state",
    "zip_code" : "customer zipcode"
    
  }

  ```
  #### Testing GET Methods ####

  for the get methods I just used a GET request on postman and changed the path in order to reflect the call I wanted
  you can download  Postman [here](https://www.postman.com/downloads/)
  
  I used following to get all entries
  - local path used: [local ip address can]/customers

  In order to get by id I simply added /[id] to the end so it looks like
  - local path used: [local ip address can]/customers/[id]

  To get all by desiered city I did essencially the same 
  - local path used: [local ip address can]/customers/[city]


### Instructions to run Locally ###
Requierments
  IMPORTANT: you will need to contact me to grant your IP access to this DB through GCP.
  Due to this being in development I have not yet opened this up to other users at the current time.
  - python see [Python Downloads](https://www.python.org/downloads/) for a  download for your OS
    -Note: For windows I found that downloading the version from Microsoft store worked best

If requirements are met you should be able to follow the remaining steps for running the local version. 
I used pipenv to set up a simple local environment and to help keep track and ensure an easy download
of all the remaining dependencies.

- clone this repository see [git documentation](https://git-scm.com/book/en/v2) for instructions.
- ensure that you download ``` pipenv ```
- run ``` pipenv shell ```
- run ``` pipenv install ```
- run ``` python app.py ```<br/>
This will start up a virtual instance and then run the following code and dependency installations on that instance. 
From this point, you will be able to test with Postman or make calls to the API you are running locally. 

## Remaining TODO ##
- [document the API methods with swagger](https://idratherbewriting.com/learnapidoc/pubapis_swagger.html)
- set up CI/CD to deploy this to GCP in some manner [general flow/ process example](https://www.youtube.com/watch?v=ab3Fca1HeEI)
- add in instructions to explain how to use without local installation
- add in ADR and architecture folder to explain decisions that were made in this project 
  to better inform future developers and help to understand the logic behind choices. 


