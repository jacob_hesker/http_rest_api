
from flask import Flask
# initializing Flask app
app = Flask(__name__)
 
# This needs to be moved into its own seperate file to help with encapsulation 
# if time allows. 
PASSWORD ="heb_testing_api"
PUBLIC_IP_ADDRESS ="35.194.8.54"
DBNAME ="customers"
PROJECT_ID ="mineral-order-320317"
INSTANCE_NAME ="heb-interview-api-1"
 
# configuration
app.config["SECRET_KEY"] = "testingsecretkey"
app.config["SQLALCHEMY_DATABASE_URI"]= f"mysql+mysqldb://root:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}?unix_socket=/cloudsql/{PROJECT_ID}:{INSTANCE_NAME}"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"]= True