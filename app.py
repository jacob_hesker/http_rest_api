from flask import Flask, request, make_response
from model import Customers, gen_response_list, gen_single_response, db
from config import app

#Get all customers in a given city.
@app.route('/customers/<city>', methods =['GET'])
def get_customers_by_city(city):
  #make sure that an entry exists with the proper city name
  exists = db.session.query(Customers.city).filter_by(city=city).first() is not None 
  #gather the customers filter out all Customers with cities not matching the one we want.
  customers = Customers.query.filter(Customers.city == city)

  if exists:
    response = gen_response_list(customers)
    
    return make_response({
        'status' : 'success',
        'message': response
    }, 200)

  else: 
    return make_response({
        'status' : 'fail',
        'message': 'No customers were found in that city.'
    }, 404)

#Get a customer based on their id. 
@app.route('/customers/<int:id>', methods =['GET'])
def get_customer_by_id(id):

  #check to see if entry exists
  exists = db.session.query(Customers.email).filter_by(id=id).first() is not None

  if exists:
    # get customer value based on the id value
    customer = Customers.query.get(id)
    #gen response
    response = gen_single_response(customer)
    return make_response({
        'status' : 'success',
        'message': response
    }, 200)
 
  else:
    return make_response({
      'status' : 'fail',
      'message': 'No such customer id exists'
    }, 404)

# Get all customers in the database 
@app.route('/customers', methods =['GET'])
def get_all_customers():
    # fetches all the customers
    customers = Customers.query.all()

    if customers:
      # response list consisting user details
      response = gen_response_list(customers)
      return make_response({
          'status' : 'success',
          'message': response
      }, 200)
      
    else:
      return make_response({
        'status' : 'fail',
        'message': 'Oops, it seems the database is Empty'
    }, 404)

# add a new customer
@app.route('/customers', methods =['POST'])
def add_new_customer():
    # check if already exists
    exists = db.session.query(Customers.email).filter_by(email=request.json['email']).first() is not None
 
    if not exists:
        try:
            # create customer object to be added
            customer = Customers(
                request.json['first_name'],
                request.json['last_name'],
                request.json['email'],
                request.json['address'],
                request.json['city'],
                request.json['state'],
                request.json['zip_code']
            )

            # add fields to customer table
            db.session.add(customer)
            db.session.commit()
            #gen response
            response = gen_single_response(customer)
            
            return make_response(response, 200)
        except FileNotFoundError:
            response = {
                'status' : 'fail',
                'message': 'Some error occured !!'
            }
            return make_response(response, 400)
         
    else:
        # if user already exists then send status as fail
        response = {
            'status' : 'fail',
            'message': 'User already exists !!'
        }
 
        return make_response(response, 403)

if __name__ == "__main__":
    # serving the app directly
    
    app.run(debug=True)
    db.create_all()