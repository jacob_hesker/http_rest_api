from flask_sqlalchemy import SQLAlchemy
from config import app
db = SQLAlchemy(app)

class Customers(db.Model):
    id = db.Column(db.Integer, primary_key = True, nullable = False)
    first_name = db.Column(db.String(50), nullable = False)
    last_name = db.Column(db.String(50), nullable = False)
    email = db.Column(db.String(100), nullable = False, unique = True)
    address = db.Column(db.String(100), nullable = False)
    city = db.Column(db.String(30), nullable = False)
    state = db.Column(db.String(20), nullable = False)
    zip_code = db.Column(db.String(7), nullable = False)

    def __init__(self,first_name,last_name,email,address,city,state,zip_code):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.address = address
        self.city = city
        self.state = state
        self.zip_code = zip_code
  
  # gerate a response for a single value

def gen_single_response(customer):
  response = {
    "id" : customer.id,
    "first_name" : customer.first_name,
    "last_name" : customer.last_name,
    "email" : customer.email,
    "address" : customer.address,
    "city" : customer.city,
    "state" : customer.state,
    "zip_code" : customer.zip_code
  }
  return response

# general method to handle the creation of a json response object
def gen_response_list(customers):
  response = list()
  for customer in customers:
    response.append({
        "id" : customer.id,
        "first_name" : customer.first_name,
        "last_name" : customer.last_name,
        "email" : customer.email,
        "address" : customer.address,
        "city" : customer.city,
        "state" : customer.state,
        "zip_code" : customer.zip_code
        })
  return response